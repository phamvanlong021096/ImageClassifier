import os

import skimage
from skimage import data

class data_loader:
    def __init__(self, data_directory):
        self.data_directory = data_directory

    def load_data(self):
        directories = [d for d in os.listdir(self.data_directory)
                       if os.path.isdir(os.path.join(self.data_directory, d))]

        images = []
        labels = []
        for d in directories:
            label_directory = os.path.join(self.data_directory, d)
            file_names = [os.path.join(label_directory, f)
                          for f in os.listdir(label_directory)
                          if f.endswith('.ppm')]

            for f in file_names:
                images.append(skimage.data.imread(f))
                labels.append(int(d))
        return images, labels