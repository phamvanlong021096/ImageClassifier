import os
import numpy as np
import tensorflow as tf
from data_loader import data_loader
from skimage import transform

ROOT_PATH = 'data'
train_data_dir = os.path.join(ROOT_PATH, 'Training')
test_data_dir = os.path.join(ROOT_PATH, 'Testing')
train_images, train_labels = data_loader(train_data_dir).load_data()
test_images, test_labels = data_loader(test_data_dir).load_data()

train_images = np.array(train_images)
train_labels = np.array(train_labels)
test_images = np.array(test_images)
test_labels = np.array(test_labels)

# resize
train_images = [transform.resize(image, (28, 28)) for image in train_images]
test_images = [transform.resize(image, (28, 28)) for image in test_images]

file_model_path = os.getcwd() + '/model.ckpt'
w_image = 28
h_image = 28
c_image = 3
batch_size = 100
test_batch_size = 100
num_filter_ic1 = 32
num_filter_ic2 = 64
num_fc1 = 700
num_fc2 = 62  # num class
reduce1x1 = 16
dropout = 0.5

graph = tf.Graph()
with graph.as_default():
    # train data and labels
    X = tf.placeholder(tf.float32, shape=[None, h_image, w_image, c_image])
    Y = tf.placeholder(tf.int32, shape=[None])
    testX = tf.placeholder(tf.float32, shape=[None, h_image, w_image, c_image])


    def create_weight(size, name):
        return tf.Variable(tf.truncated_normal(size, stddev=0.1), name=name)


    def create_bias(size, name):
        return tf.Variable(tf.constant(0.1, shape=size), name=name)


    def conv2d(x, W):
        return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


    def max_pool_3x3(x):
        return tf.nn.max_pool(x, ksize=[1, 3, 3, 1], strides=[1, 1, 1, 1], padding='SAME')


    # Inception Module1
    #
    # follows input
    W_conv1_1x1_1 = create_weight([1, 1, c_image, num_filter_ic1], 'W_conv1_1x1_1')
    b_conv1_1x1_1 = create_weight([num_filter_ic1], 'b_conv1_1x1_1')

    W_conv1_1x1_2 = create_weight([1, 1, c_image, reduce1x1], 'W_conv1_1x1_2')
    b_conv1_1x1_2 = create_weight([reduce1x1], 'b_conv1_1x1_2')

    W_conv1_1x1_3 = create_weight([1, 1, c_image, reduce1x1], 'W_conv1_1x1_3')
    b_conv1_1x1_3 = create_weight([reduce1x1], 'b_conv1_1x1_3')

    # follows 1x1_2
    W_conv1_3x3 = create_weight([3, 3, reduce1x1, num_filter_ic1], 'W_conv1_3x3')
    b_conv1_3x3 = create_weight([num_filter_ic1], 'b_conv1_3x3')

    # follows 1x1_3
    W_conv1_5x5 = create_weight([5, 5, reduce1x1, num_filter_ic1], 'W_conv1_5x5')
    b_conv1_5x5 = create_bias([num_filter_ic1], 'b_conv1_5x5')

    # follows max pooling
    W_conv1_1x1_4 = create_weight([1, 1, c_image, num_filter_ic1], 'W_conv1_1x1_4')
    b_conv1_1x1_4 = create_weight([num_filter_ic1], 'b_conv1_1x1_4')

    # Inception Module2
    #
    # follows inception 1
    W_conv2_1x1_1 = create_weight([1, 1, 4 * num_filter_ic1, num_filter_ic2], 'W_conv2_1x1_1')
    b_conv2_1x1_1 = create_weight([num_filter_ic2], 'b_conv2_1x1_1')

    W_conv2_1x1_2 = create_weight([1, 1, 4 * num_filter_ic1, reduce1x1], 'W_conv2_1x1_2')
    b_conv2_1x1_2 = create_weight([reduce1x1], 'b_conv2_1x1_2')

    W_conv2_1x1_3 = create_weight([1, 1, 4 * num_filter_ic1, reduce1x1], 'W_conv2_1x1_3')
    b_conv2_1x1_3 = create_weight([reduce1x1], 'b_conv2_1x1_3')

    # follows 1x1_2
    W_conv2_3x3 = create_weight([3, 3, reduce1x1, num_filter_ic2], 'W_conv2_3x3')
    b_conv2_3x3 = create_weight([num_filter_ic2], 'b_conv2_3x3')

    # follows 1x1_3
    W_conv2_5x5 = create_weight([5, 5, reduce1x1, num_filter_ic2], 'W_conv2_5x5')
    b_conv2_5x5 = create_bias([num_filter_ic2], 'b_conv2_5x5')

    # follows max pooling
    W_conv2_1x1_4 = create_weight([1, 1, 4 * num_filter_ic1, num_filter_ic2], 'W_conv2_1x1_4')
    b_conv2_1x1_4 = create_weight([num_filter_ic2], 'b_conv2_1x1_4')

    # Fully connected layers
    W_fc1 = create_weight([w_image * h_image * num_filter_ic2 * 4, num_fc1], 'W_fc1')
    b_fc1 = create_bias([num_fc1], 'b_fc1')

    W_fc2 = create_weight([num_fc1, num_fc2], 'W_fc2')
    b_fc2 = create_bias([num_fc2], 'b_fc2')


    def model(x, train=True):
        # Inception Module 1
        conv1_1x1_1 = conv2d(x, W_conv1_1x1_1) + b_conv1_1x1_1
        conv1_1x1_2 = tf.nn.relu(conv2d(x, W_conv1_1x1_2) + b_conv1_1x1_2)
        conv1_1x1_3 = tf.nn.relu(conv2d(x, W_conv1_1x1_3) + b_conv1_1x1_3)
        conv1_3x3 = conv2d(conv1_1x1_2, W_conv1_3x3) + b_conv1_3x3
        conv1_5x5 = conv2d(conv1_1x1_3, W_conv1_5x5) + b_conv1_5x5
        maxpool1 = max_pool_3x3(x)
        conv1_1x1_4 = conv2d(maxpool1, W_conv1_1x1_4) + b_conv1_1x1_4

        # concatenate all the feature maps and hit them with a relu
        inception1 = tf.nn.relu(tf.concat([conv1_1x1_1, conv1_3x3, conv1_5x5, conv1_1x1_4], 3))

        # Inception Module 2
        conv2_1x1_1 = conv2d(inception1, W_conv2_1x1_1) + b_conv2_1x1_1
        conv2_1x1_2 = tf.nn.relu(conv2d(inception1, W_conv2_1x1_2) + b_conv2_1x1_2)
        conv2_1x1_3 = tf.nn.relu(conv2d(inception1, W_conv2_1x1_3) + b_conv2_1x1_3)
        conv2_3x3 = conv2d(conv2_1x1_2, W_conv2_3x3) + b_conv2_3x3
        conv2_5x5 = conv2d(conv2_1x1_3, W_conv2_5x5) + b_conv2_5x5
        maxpool2 = max_pool_3x3(inception1)
        conv2_1x1_4 = conv2d(maxpool2, W_conv2_1x1_4) + b_conv2_1x1_4

        # concatenate all the feature maps and hit them with a relu
        inception2 = tf.nn.relu(tf.concat([conv2_1x1_1, conv2_3x3, conv2_5x5, conv2_1x1_4], 3))

        # flatten features for fully connected layer
        inception2_flat = tf.reshape(inception2, [-1, w_image * h_image * num_filter_ic2 * 4])

        # fully connected layers
        if train:
            h_fc1 = tf.nn.dropout(tf.nn.relu(tf.matmul(inception2_flat, W_fc1) + b_fc1), keep_prob=dropout)
        else:
            h_fc1 = tf.nn.relu(tf.matmul(inception2_flat, W_fc1) + b_fc1)
        return tf.matmul(h_fc1, W_fc2) + b_fc2


    loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels=Y, logits=model(X)))
    opt = tf.train.AdamOptimizer(1e-4).minimize(loss)

    prediction_test = tf.nn.softmax(model(testX, train=False))

    # initialize variable
    init = tf.global_variables_initializer()

    # use to save variables so we can pick up later
    saver = tf.train.Saver()

num_steps = 50
sess = tf.Session(graph=graph)

# initialize variables
sess.run(init)
print('Model initialized.')

# set use_previous=1 to use file_path model
# set use_previous=0 to start model from scratch
use_previous = 1

if use_previous:
    saver.restore(sess, file_model_path)
    print("Model restored.")

# training
num_batch = (len(train_images) - 1) // batch_size + 1
loss_value = 0
for s in range(num_steps):
    for i in range(num_batch):
        start_batch = i * batch_size
        end_batch = min(start_batch + batch_size, len(train_images))
        #print("start batch: ", start_batch)
        #print("end batch:", end_batch)
        batch_X = train_images[start_batch:end_batch]
        batch_Y = train_labels[start_batch:end_batch]
        _, loss_value = sess.run([opt, loss], feed_dict={X: batch_X, Y: batch_Y})
    if s % 10 == 0:
        print("Loss after {0} steps: {1}".format(s, loss_value))

# save model
save_path = saver.save(sess, file_model_path)
print("Model saved.")


# test
def num_true_prediction(predict, labels):
    res = 0
    for i in range(len(predict)):
        if np.argmax(predict[i]) == labels[i]:
            res += 1
    return res


accuracy = 0
num_test_batch = (len(test_images) - 1) // test_batch_size + 1
for i in range(num_test_batch):
    start_batch = i * test_batch_size
    end_batch = min(start_batch + test_batch_size, len(test_images))
    test_batch_X = test_images[start_batch: end_batch]
    test_batch_Y = test_labels[start_batch: end_batch]
    prediction = sess.run(prediction_test, feed_dict={testX: test_batch_X})
    accuracy += num_true_prediction(prediction, test_batch_Y)

print("Test accuracy: " + str(100 * accuracy / len(test_images)))
